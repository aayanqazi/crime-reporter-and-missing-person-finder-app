import {bindActionCreators} from "redux"
import {connect} from "react-redux"
import App from "../Components/App"
import {firedux} from "../Server/index"
import {signUp} from "../Actions/actions"
import {Login} from "../Actions/actions"
import {Logins} from "../Reducer/Login"
import {Details} from "../Reducer/Detail"
import {signOut} from "../Actions/actions"
import {fetchReports} from "../Actions/actions"
import {complains} from "../Actions/actions"
import {Missing} from "../Actions/actions"
import {Crimes} from "../Actions/actions"
import {fetchcrimeReports} from "../Actions/actions"
import {fetchclaimReports} from "../Actions/actions"
import {fetchclaimReportsAdmin} from "../Actions/actions"

function mapStateToProps(state) {
   return {
     login : state.Logins,
     fire: state.firedux,
     Detail: state.Details,
     Crime: state.Crime,
     Complains: state.Complains
     
   }
}
const mapDispatchToProps = (dispatch) => ({
  signUp: bindActionCreators(signUp, dispatch),
  Login: bindActionCreators(Login,dispatch),
  signOut: bindActionCreators(signOut,dispatch),
  fetchReport: bindActionCreators(fetchReports,dispatch),
  complain: bindActionCreators(complains,dispatch),
  crime: bindActionCreators(Crimes,dispatch),
  missing: bindActionCreators(Missing,dispatch),
  fetchCrime: bindActionCreators(fetchcrimeReports,dispatch),
  fetchClaims: bindActionCreators(fetchclaimReports,dispatch),
  fetchClaimsAdmin:bindActionCreators(fetchclaimReportsAdmin,dispatch)
});
const Main = connect(mapStateToProps,mapDispatchToProps)(App)

export default Main