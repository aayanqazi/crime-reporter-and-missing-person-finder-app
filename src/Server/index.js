import firebase from "firebase"
import Firedux from "firedux"
var config = {
    apiKey: "AIzaSyC25DmjbdboIlzOsIePv3TGyOwBoHB8qOQ",
    authDomain: "slider-2b765.firebaseapp.com",
    databaseURL: "https://slider-2b765.firebaseio.com",
    storageBucket: "slider-2b765.appspot.com",
    messagingSenderId: "112199883944"
  };
  firebase.initializeApp(config);


const database = firebase.database();
var ref = database.ref()
export const firedux = new Firedux({
    ref,
    // Optional: 
    omit: ['$localState'] // Properties to reserve for local use and not sync with Firebase. 
})

export default database