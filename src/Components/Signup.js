import React, { Component } from 'react';
import { Link } from "react-router"
import RaisedButton from 'material-ui/RaisedButton';
import injectTapEventPlugin from 'react-tap-event-plugin';
import TextField from 'material-ui/TextField';


import Paper from 'material-ui/Paper';
class Signup extends React.Component {
  state ={
    Data:{}
  }
    singUp = (ev)=>
      { 
        ev.preventDefault();
        this.props.signUp(this.refs.firstName.getValue(),this.refs.email.getValue(),this.refs.password.getValue()) 
      }



  render() {
    return <div style={{margin:"0 , auto"}}> 
      <br/>
      <br />
      <center>
      <h1> Registration </h1>
         <form onSubmit={this.singUp}><TextField
      hintText="Full Name" required
      ref = "firstName"
    /> <br />
    <TextField
      hintText="Email"
      type="email"
      ref = "email"
      required
    />
    <br />
    <TextField
      hintText="Password"
      type="password"
      ref = "password"
      required
    />
    <br />
      <RaisedButton label="Sign Up" type="submit" primary={true} style={{margin:"12px"}}/>
   <Link to="/" ><RaisedButton label="Cancel" primary={true} /></Link>

    </form>

        </center>
       </div>
  }
}



injectTapEventPlugin();
export default Signup;

