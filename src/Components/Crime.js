import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import firebase from "firebase"
import { browserHistory } from "react-router"
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import ImageUploader from 'react-firebase-image-uploader';

class Complains extends Component {
    state = {
        city: "",
        app: "New York",
        open: true
    }

    handleChangess = (event, index, city) => this.setState({ city });
    clickme = (ev) => {
        ev.preventDefault()
        var id = this.refs.id.getValue()
        var subject = this.refs.subject.getValue()
        var crime = this.refs.crime.getValue()
        var obj = Object.assign({ id: id, subject: subject, crime: crime, city: this.state.city, status: "Pending ..." })
        var uid = firebase.auth().currentUser.uid
        this.props.crime(uid, obj)
        
       
    }
    componentWillMount() {
        var app = Math.random().toString(36).substr(2, 9);
        this.setState({ app: app })
    }
   

    close = () => {
        browserHistory.push("/dashboard")
    }
    render() {

        return (
            <div className="App">
                <form onSubmit={this.clickme}>
                    <center>
                        <h2>Crime Report </h2><br /><br />
                        <TextField
                            value={this.state.app}
                            disabled
                            ref="id"
                            floatingLabelText="Unique Case #"
                        /><br />
                        <SelectField
                            floatingLabelText="City"
                            value={this.state.city}
                            onChange={this.handleChangess}
                        >
                            <MenuItem value={"New York"} primaryText="New York" />
                            <MenuItem value={"California"} primaryText="California" />
                            <MenuItem value={"Chicago"} primaryText="Chicago" />
                            <MenuItem value={"Houston"} primaryText="Houston" />
                            <MenuItem value={"Philadelphia"} primaryText="Philadelphia" />
                        </SelectField> <br />
                        <TextField
                            hintText="Subject"
                            ref="subject"
                            required
                            floatingLabelText="Subject"
                        /><br /><TextField
                            hintText="Description"
                            ref="crime"
                            required
                            floatingLabelText="Description"
                        /><br />
                         
                        <RaisedButton type="submit" label="Submit" primary={true} style={{ margin: "10px" }} />
                        <RaisedButton label="Cancel" primary={true} onTouchTap={this.close} />
                    </center>

                </form><br />

            </div>
        );
    }
}

export default Complains;
