import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import firebase from "firebase"
import { browserHistory } from "react-router"
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FileUploader from 'react-firebase-file-uploader';
import {Link} from "react-router"
class Missing extends Component {
    state = {
        city: "",
        app: "New York",
        open: true,
        avatar: '',
      isUploading: false,
      progress: 0,
      avatarURL: ''
    }

    handleChangess = (event, index, city) => this.setState({ city });
    clickme = (ev) => {
        ev.preventDefault()
        var id = this.refs.id.getValue()
        var subject = this.refs.subject.getValue()
        var complains = this.refs.complains.getValue()
        var obj = Object.assign({ id: id, subject: subject, report: complains, city: this.state.city,url:this.state.avatarURL, status: "Pending ..." })
        var uid = firebase.auth().currentUser.uid
        this.props.missing(uid, obj)
       
    }
    componentWillMount() {
        var app = Math.random().toString(36).substr(2, 9);
        this.setState({ app: app })
    }
   
 handleChangeUsername = (event) => this.setState({username: event.target.value});
  handleUploadStart = () => this.setState({isUploading: true, progress: 0});
  handleProgress = (progress) => this.setState({progress});
  handleUploadError = (error) => {
      this.setState({isUploading: false});
      console.error(error);
  }
  handleUploadSuccess = (filename) => {
      this.setState({avatar: filename, progress: 100, isUploading: false});
      firebase.storage().ref('images').child(filename).getDownloadURL().then(url => this.setState({avatarURL: url}));
  };

    close = () => {
        browserHistory.push("/dashboard")
    }
    render() {

        return (
            <div className="App">
                                <Link to="/dashboard"><RaisedButton label="back" primary={true} onTouchTap={this.close} /></Link>

                <form onSubmit={this.clickme}>
                    <center>
                        <h2>Missing Report </h2><br /><br />
                        <TextField
                            value={this.state.app}
                            disabled
                            ref="id"
                            floatingLabelText="Unique Case #"
                        /><br />
                        <SelectField
                            floatingLabelText="City"
                            value={this.state.city}
                            onChange={this.handleChangess}
                        >
                            <MenuItem value={"New York"} primaryText="New York" />
                            <MenuItem value={"California"} primaryText="California" />
                            <MenuItem value={"Chicago"} primaryText="Chicago" />
                            <MenuItem value={"Houston"} primaryText="Houston" />
                            <MenuItem value={"Philadelphia"} primaryText="Philadelphia" />
                        </SelectField> <br />
                        <TextField
                            hintText="Subject"
                            ref="subject"
                            required
                            floatingLabelText="Subject"
                        /><br /><TextField
                            hintText="Description"
                            ref="complains"
                            required
                            floatingLabelText="Description"
                        /><br /><br />
                        
          <label>Picture:</label><br />
          <br />
          {this.state.isUploading &&
            <p>Progress: {this.state.progress}</p>
          }
          {this.state.avatarURL &&
            <img src={this.state.avatarURL} style={{width:"300px"}} />
          }<br /><br />
          <FileUploader
            accept="image/*"
            name="avatar"
            randomizeFilename
            storageRef={firebase.storage().ref('images')}
            onUploadStart={this.handleUploadStart}
            onUploadError={this.handleUploadError}
            onUploadSuccess={this.handleUploadSuccess}
            onProgress={this.handleProgress}
            required
          />
                  <br />
                        <RaisedButton type="submit" label="Submit" primary={true} style={{ margin: "10px" }} />
                        <RaisedButton label="Cancel" primary={true} onTouchTap={this.close} />
                    </center>
                </form><br />

            </div>
        );
    }
}

export default Missing;
