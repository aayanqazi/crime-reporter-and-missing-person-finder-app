import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import Chip from 'material-ui/Chip';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import Toggle from 'material-ui/Toggle';
import MenuItem from 'material-ui/MenuItem';
import { List, ListItem } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import firebase from "firebase"
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import {Link} from "react-router"
class crimeReport extends Component {
    state = {
        open: false
    };
    componentWillMount() {
        this.props.fetchClaims(firebase.auth().currentUser.uid)
    }
    componentWillUpdate() {

    }



    render() {

        var obj = this.props.Complains
        var newObj = {}
        var arrKey = []
        return (<div>
                <Link to="/dashboard"><RaisedButton label="back" primary={true} onTouchTap={this.close} /></Link>

            <Table>
                <TableHeader>
                    <TableRow>
                         <TableHeaderColumn>ID</TableHeaderColumn>
                            <TableHeaderColumn>Subject</TableHeaderColumn>
                            <TableHeaderColumn>Description</TableHeaderColumn>
                           <TableHeaderColumn>City</TableHeaderColumn>
                            <TableHeaderColumn>Status</TableHeaderColumn>
                            <TableHeaderColumn>Action</TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody>
                    {obj.map((arr) => {
                        for (var key in arr) {
                            arrKey.push(key)
                            console.log(key)
                            newObj = Object.assign(arr, newObj)
                        }
                    })}

                    {arrKey.map((res) => {
                        return <TableRow>
                            <TableRowColumn>{newObj[res].id}</TableRowColumn>
                                <TableRowColumn>{newObj[res].subject}</TableRowColumn>
                                <TableRowColumn>{newObj[res].complain}</TableRowColumn>
                                <TableRowColumn>{newObj[res].city}</TableRowColumn>
                                <TableRowColumn>{newObj[res].status}</TableRowColumn>
                                 <TableRowColumn></TableRowColumn>
                        </TableRow>
                    })}


                </TableBody>
            </Table>
        </div>
        );
    }
}
export default crimeReport;
