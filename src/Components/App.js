import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import Chip from 'material-ui/Chip';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import Toggle from 'material-ui/Toggle';
import {Link} from "react-router"
import TextField from 'material-ui/TextField';

class App extends Component {
  state = {
    open: false,
    openAdmin:false
  };
 componentWillMount()
 {
   this.props.fetchReport()
 }
 componentWillUpdate(){
   var arr = this.props.Detail
 }
  handleOpen = () => {
    this.setState({ open: true });
  };
  handleClose = () => {
    this.setState({ open: false });
  };
  handleCloseAdmin = () => {
    this.setState({ openAdmin: false });
  };
   handleOpenAdmin = () => {
    this.setState({ openAdmin: true });
  };
  handleAdmin = (ev)=>{
    ev.preventDefault();
     var email = this.refs.email.getValue()
    var password = this.refs.password.getValue()
    this.props.Login(email, password)
    this.setState({
      openAdmin:false
    })
    console.log(this.refs.email.getValue())
  }
  Closing = () => {
    this.props.signOut()
    this.setState({ open: false });
    
  };
 
    handleChange = (event, DONOR) => {
    this.setState({donorChange: DONOR});
  };
  

  render() {
    const actions = [
        <FlatButton
        label="SignOut"
        primary={true}
        onTouchTap={this.Closing}
      />,
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
      />

    ];
     const actionsAdmin = [
    <RaisedButton label="Cancel" onTouchTap={this.handleCloseAdmin} primary={true} />


    ];
    var obj ={}
    var arrKey = []
    return (
    
      <div>
         
        {this.props.login.isLogged ? <AppBar
          style={{textAlign: "center" }}
          title="Crime Reporter and Missing Person Finder App "
          
          iconElementRight={<Chip onTouchTap={this.handleOpen}>{this.props.login.user.email}</Chip>}
          iconClassNameRight="muidocs-icon-navigation-expand-more"
          onRightIconButtonTouchTap={this.handleToggle}
        /> : <AppBar
            style={{textAlign: "center" }}
            title="Crime Reporter and Missing Person Finder App "
            iconClassNameRight="muidocs-icon-navigation-expand-more"
            iconElementRight={<Link to="/"><RaisedButton label={"Admin Area"} onTouchTap={this.handleOpenAdmin} secondary={true} /></Link>}
          />}
          <br/>
          {this.props.Detail.map((arr)=>{
            for (var key in arr)
            {
              obj = Object.assign(arr[key],obj)
            }
          arrKey = Object.keys(obj)
          })}
           
        <Dialog
          title="What you want to do ??"
          modal={false}
          actions={actions}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          Do you want to signOut ??? 
        </Dialog>
          <Dialog
          title="ADMIN AREA"
          style={{textAlign:"center"}}
          modal={false}
          actions={actionsAdmin}
          open={this.state.openAdmin}
          onRequestClose={this.handleCloseAdmin}
        >
          <form onSubmit={this.handleAdmin} style={{ width: '100%', maxWidth: 700, margin: 'auto' }} >
        <h1>Login </h1>
        <TextField
          hintText="Enter your email Address"
          floatingLabelText="email"
          type="email"
         
          multiLine={false} ref="email" required /><br />
        <TextField
          hintText="Enter Your Password"
          floatingLabelText="Password"
          type="password"
         
          multiLine={false} ref="password" required /> <br /><br />
        <RaisedButton
          label="Login"
          primary={true}
          type="submit"
           style={{margin:"12px"}}
        />
      </form>
        </Dialog>
      <center>
        <Link to="/report-missing"><RaisedButton label="MIssing Report" primary={true} style={{margin:"10px"}} /></Link> <br />       
         <Link to="/crime-report"><RaisedButton label="Crime Report" primary={true} style={{margin:"10px"}} /></Link>      </center>

        {React.cloneElement(this.props.children, this.props)}
      </div>
    );
  }
}
export default App;
