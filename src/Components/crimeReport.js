import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import Chip from 'material-ui/Chip';
import Avatar from 'material-ui/Avatar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import Toggle from 'material-ui/Toggle';
import MenuItem from 'material-ui/MenuItem';
import { List, ListItem } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import { Link } from "react-router"
import firebase from "firebase"
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';

class crimeReport extends Component {
    state = {
        open: false,
        admin: false
    };
    componentWillMount() {

              this.props.fetchCrime()


      
    }
    componentWillUpdate() {
        var arr = this.props.Crime
    }



    render() {

        var obj = {}
        var arrKey = []
        return (

            <div>
<Link to="/dashboard"><RaisedButton label="back" primary={true} onTouchTap={this.close} /></Link>

                
                <Table>
                    <TableHeader>
                        <TableRow>
                            <TableHeaderColumn>ID</TableHeaderColumn>
                            <TableHeaderColumn>Subject</TableHeaderColumn>
                            <TableHeaderColumn>Description</TableHeaderColumn>
                           <TableHeaderColumn>City</TableHeaderColumn>
                            <TableHeaderColumn>Status</TableHeaderColumn>
                            <TableHeaderColumn>Action</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody>
                        {this.props.Crime.map((arr) => {
                            for (var key in arr) {
                                obj = Object.assign(arr[key], obj)
                            }
                            arrKey = Object.keys(obj)
                        })}

                        {arrKey.map((res) => {
                            if(this.state.admin)
                            {
                            if(obj[res].status==="Solved")
                                {
                                    var green = <TableHeaderColumn><RaisedButton label="Solved" disabled /></TableHeaderColumn>

                                }
                                else
                                {
                                   var green = <TableHeaderColumn><RaisedButton label="Solved" /></TableHeaderColumn>

                                }
                            }
                            else
                            {
                                var green = ""
                            }
                            return (<TableRow>
                                <TableRowColumn>{obj[res].id}</TableRowColumn>
                                <TableRowColumn>{obj[res].subject}</TableRowColumn>
                                <TableRowColumn>{obj[res].crime}</TableRowColumn>
                                <TableRowColumn>{obj[res].city}</TableRowColumn>
                                <TableRowColumn>{obj[res].status}</TableRowColumn>
                                
                                 <TableRowColumn>{green}</TableRowColumn>
                            </TableRow>)
                        })}
                        
                    </TableBody>
                </Table>
            </div>
        );
    }
}
export default crimeReport;
