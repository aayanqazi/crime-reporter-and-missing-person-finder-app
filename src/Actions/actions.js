import firebase from "firebase"
import database from "../Server/index"
import { browserHistory } from "react-router"
import { store } from "../Store/store"
import Snackbar from 'material-ui/Snackbar';
export function signUp(userDetail, email,password) {
    return (dispatch) => {
        return firebase.auth().createUserWithEmailAndPassword(email, password).then(function (result) {
            console.log("SuccessfuLly")
            return insertUserDetail(userDetail, result.uid,email)
        }).catch(function (error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            alert(errorMessage)
        });
    }
    
}

function insertUserDetail(userDetail, uid,email) {
    var obj = Object.assign({email:email,fullName:userDetail})
    console.log(uid)
    console.log(userDetail)
    database.ref().child("users/" + uid).set(obj).then((result) => {
        console.log("Successfully COmpleted")
    })
}

export function Login(email, password) {
    return (dispatch) => {
        return firebase.auth().signInWithEmailAndPassword(email, password)
            .then((user) => {
                return dispatch({
                    type: "LoginSuccess",
                    user
                })
                
            })

            .catch(function (error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                alert(errorMessage)
            });
    }
}



export function signOut() {
    return (dispatch) => {
        return firebase.auth().signOut().then(function (user) {
            browserHistory.push("/")
            return dispatch({
                type: "logout"
            })
            
        }, function (error) {
            console.log(error)

        });
    }
}

export function Crimes(uid,data)
{
    return (dispatch)=>{
        return database.ref().child("crimes/" + uid).push(data).then((result) => {
        browserHistory.push("/dashboard")
    })
    }
}
export function fetchBloodGroup(uid) {
    return (dispatch) => {
        return database.ref().child("users/" + uid).once("value").then((result) => {
            var blood = result.val().bloodGroup

        })
        
    }
}


export function fetchReports() {
    return (dispatch) => {
        return database.ref().child("missing").once("value").then((result) => {
            var dataz = result.val()
            return dispatch({
                type:"Reports",
                dataz
            })
        })
        
    }
}

export function fetchcrimeReports() {
    return (dispatch) => {
        return database.ref().child("crimes").once("value").then((result) => {
            var datass = result.val()
            return dispatch({
                type:"Crime",
                datass
            })
        })
        
    }
}

export function fetchclaimReports(uid) {
    return (dispatch) => {
        return database.ref().child("complains/"+ uid).once("value").then((result) => {
            var data = result.val()
            return dispatch({
                type:"Complains",
                data
            })
        })
        
    }
}

export function fetchclaimReportsAdmin(){
    return (dispatch) => {
        return database.ref().child("complains/").once("value").then((result) => {
            var data = result.val()
            console.log(result.val())
            return dispatch({
                type:"Complains",
                data
            })
        })
        
        
    }
}

export function complains(uid,data)
{
    return (dispatch)=>{
        return database.ref().child("complains/" + uid).push(data).then((result) => {
        browserHistory.push("/dashboard")
    })
    }

}

export function Missing(uid,data)
{
    return (dispatch)=>{
        return database.ref().child("missing/" + uid).push(data).then((result) => {
        browserHistory.push("/dashboard")
    })
    }
}