import {combineReducers} from "redux"
import {routerReducer} from "react-router-redux"
import {firedux} from '../Server/index'
import {Logins} from "./Login" 
import {Details} from "./Detail"
import {Crime} from "./Crime"
import {Complains} from "./Complains"

const rootReducer = combineReducers({
    Logins,
    Details,
    Crime,
    Complains,
    firedux: firedux.reducer(),
    routing: routerReducer
})
export default rootReducer